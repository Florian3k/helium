open TypingStructure

type t

val empty : t

val add_tvar  : t -> 'k tvar -> t
val add_tvars : t -> TVar.ex list -> t

val add_effinst : t -> effinst -> t

val has_tvar    : t -> 'k tvar -> bool
val has_effinst : t -> effinst -> bool
