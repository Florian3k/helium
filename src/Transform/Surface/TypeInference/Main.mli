
val tr_program   : Lang.Surface.source_file -> Lang.Unif.source_file
val tr_interface : Lang.Surface.intf_file -> Lang.Unif.intf_file

val source_flow_tag : Flow.tag
val intf_flow_tag   : Flow.tag
