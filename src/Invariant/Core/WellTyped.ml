open Lang.Node
open Lang.Core

exception TypeError

let log_b = Log.log_b "Invariant.Core.WellTyped"

module Env : sig
  type t
  val empty : t
  val add_tvar   : t -> 'k tvar -> t * 'k tvar
  val add_tvars  : t -> TVar.ex list -> t * TVar.ex list
  val add_tvars2 : t -> TVar.ex list -> TVar.ex list -> t * subst

  val add_effinst : t -> effinst -> effsig -> t * effinst

  val add_var  : t -> var -> ttype -> t
  val add_vars : t -> var list -> ttype list -> t

  val scope    : t -> scope
  val to_subst : t -> subst

  val tr_tvar     : t -> 'k tvar -> 'k tvar
  val lookup_inst : t -> effinst -> effinst * effsig
  val check_var   : t -> var -> ttype
end = struct
  module TVarMap = TVar.Map.Make(TVar)
  type t =
    { type_env : TVarMap.t
    ; inst_env : (effinst * effsig) EffInst.Map.t
    ; var_env  : ttype Var.Map.t
    ; scope    : scope
    }

  let empty =
    { type_env = TVarMap.empty
    ; inst_env = EffInst.Map.empty
    ; var_env  = Var.Map.empty
    ; scope    = Scope.empty
    }

  let add_tvar env x =
    let y = TVar.clone x in
    { env with
      type_env = TVarMap.add x y env.type_env
    ; scope    = Scope.add_tvar env.scope y
    }, y

  let add_tvar_ex env (TVar.Pack x) =
    let (env, x) = add_tvar env x in
    (env, TVar.Pack x)

  let add_tvars env xs =
    Utils.ListExt.fold_map add_tvar_ex env xs

  let rec add_tvars2_aux env sub xs ys =
    match xs, ys with
    | [], [] -> (env, sub)
    | TVar.Pack x :: xs, TVar.Pack y :: ys ->
      begin match Kind.equal (TVar.kind x) (TVar.kind y) with
      | Equal ->
        let (env, z) = add_tvar env x in
        add_tvars2_aux env (Subst.add_type sub y (Type.var z)) xs ys
      | NotEqual -> raise TypeError
      end
    | [], _ :: _ | _ :: _, [] -> raise TypeError

  let add_tvars2 env xs ys =
    add_tvars2_aux env Subst.empty xs ys

  let add_effinst env a s =
    let b = EffInst.clone a in
    { env with
      inst_env = EffInst.Map.add a (b, s) env.inst_env
    ; scope    = Scope.add_effinst env.scope b
    }, b

  let add_var env x tp =
    { env with
      var_env = Var.Map.add x tp env.var_env
    }

  let add_vars env xs tps =
    if List.length xs <> List.length tps then
      raise TypeError
    else List.fold_left2 add_var env xs tps
  
  let scope env = env.scope

  let to_subst env =
    let sub = Subst.empty in
    let sub = TVarMap.fold
      { fold_f = fun x y sub -> Subst.add_type sub x (Type.var y) }
      env.type_env
      sub in
    EffInst.Map.fold
      (fun a (b, _) sub -> Subst.add_inst sub a b)
      env.inst_env
      sub

  let tr_tvar env x =
    match TVarMap.find_opt x env.type_env with
    | None -> raise TypeError
    | Some x -> x

  let lookup_inst env a =
    match EffInst.Map.find_opt a env.inst_env with
    | None -> raise TypeError
    | Some r -> r

  let check_var env x =
    match Var.Map.find_opt x env.var_env with
    | None -> raise TypeError
    | Some x -> x
end

(* ========================================================================= *)

let rec tr_ctor_decl env ctor =
  match ctor with
  | CtorDecl(name, xs, tps) ->
    let (env, xs) = Env.add_tvars env xs in
    CtorDecl(name, xs, List.map (tr_type env) tps)

and tr_field_decl env fld =
  match fld with
  | FieldDecl(name, tp) ->
    FieldDecl(name, tr_type env tp)

and tr_op_decl env op =
  match op with
  | OpDecl(name, xs, tps, tp) ->
    let (env, xs) = Env.add_tvars env xs in
    OpDecl(name, xs, List.map (tr_type env) tps, tr_type env tp)

and tr_type : type k. Env.t -> k typ -> k typ =
  fun env tp ->
  match Type.view tp with
  | TEffPure     -> Type.eff_pure
  | TEffCons(eff1, eff2) ->
    Type.eff_cons (tr_type env eff1) (tr_type env eff2)
  | TEffInst a ->
    Type.eff_inst (fst (Env.lookup_inst env a))
  | TBase    b   -> Type.base b
  | TNeutral neu ->
    Type.neutral (tr_neutral_type env neu)
  | TTuple tps ->
    Type.tuple (List.map (tr_type env) tps)
  | TArrow(tp1, tp2, eff) ->
    Type.arrow (tr_type env tp1) (tr_type env tp2) (tr_type env eff)
  | TForall(x, tp) ->
    let (env, x) = Env.add_tvar env x in
    Type.forall x (tr_type env tp)
  | TExists(x, tp) ->
    let (env, x) = Env.add_tvar env x in
    Type.exists x (tr_type env tp)
  | TForallInst(a, s, tp) ->
    let s = tr_type env s in
    let (env, a) = Env.add_effinst env a s in
    Type.forall_inst a s (tr_type env tp)
  | TDataDef(tp, ctors) ->
    Type.data_def (tr_type env tp) (List.map (tr_ctor_decl env) ctors)
  | TRecordDef(tp, flds) ->
    Type.record_def (tr_type env tp) (List.map (tr_field_decl env) flds)
  | TEffsigDef(s, ops) ->
    Type.effsig_def (tr_type env s) (List.map (tr_op_decl env) ops)
  | TFun(x, tp) ->
    let (env, x) = Env.add_tvar env x in
    Type.tfun x (tr_type env tp)
  | TFuture fut ->
    Type.future fut.future
      (Subst.compose (Env.to_subst env) fut.subst)

and tr_neutral_type : type k. Env.t -> k neutral_type -> k neutral_type =
  fun env neu ->
  match neu with
  | TVar x -> TVar (Env.tr_tvar env x)
  | TApp(neu, tp) ->
    TApp(tr_neutral_type env neu, tr_type env tp)

(* ========================================================================= *)

let rec check_type_args env xs args sub =
  match xs, args with
  | [], [] -> sub
  | TVar.Pack x :: xs, TpArg tp :: args ->
    begin match Kind.equal (TVar.kind x) (Type.kind tp) with
    | Equal ->
      check_type_args env xs args (Subst.add_type sub x (tr_type env tp))
    | NotEqual -> raise TypeError
    end
  | [], _ :: _ | _ :: _, [] -> raise TypeError

let check_typedefs env tds =
  let module M = struct
    type td = TD : 'k tvar * var * ttype -> td

    let prepare_typedef env (TypeDef(x, z, tp)) =
      let (env, x) = Env.add_tvar env x in
      (env, TD(x, z, tp))

    let rec check_typedef_shape : type k. k neutral_type -> ttype -> unit =
      fun td tp ->
        match Type.neutral_kind td with
        | KType ->
          begin match Type.view tp with
          | TDataDef(td', _) | TRecordDef(td', _) ->
            if Type.equal (Type.neutral td) td' then ()
            else raise TypeError
          | _ -> raise TypeError
          end
        | KEffect -> raise TypeError
        | KEffsig ->
          begin match Type.view tp with
          | TEffsigDef(td', _) ->
            if Type.equal (Type.neutral td) td' then ()
            else raise TypeError
          | _ -> raise TypeError
          end
        | KArrow(k1, _) ->
          begin match Type.view tp with
          | TForall(x, tp) ->
            begin match Kind.equal k1 (TVar.kind x) with
            | Equal ->
              check_typedef_shape (TApp(td, Type.var x)) tp
            | NotEqual -> raise TypeError
            end
          | _ -> raise TypeError
          end

    let check_typedef env (TD(x, z, tp)) =
      let tp = tr_type env tp in
      check_typedef_shape (TVar x) tp;
      Env.add_var env z tp
  end in
  let (env, tds) = Utils.ListExt.fold_map M.prepare_typedef env tds in
  List.fold_left M.check_typedef env tds

(* ========================================================================= *)

let rec check_expr env e =
  match e.data with
  | EValue v -> (check_value env v, Type.eff_pure)
  | ELet(x, e1, e2) ->
    let (tp1, eff1) = check_expr env e1 in
    let (tp2, eff2) = check_expr (Env.add_var env x tp1) e2 in
    (tp2, Type.eff_cons eff1 eff2)
  | ELetPure(x, e1, e2) ->
    let tp1 = check_expr_eff env e1 Type.eff_pure in
    check_expr (Env.add_var env x tp1) e2
  | EFix(rfs, e2) ->
    let env = check_rec_functions env rfs in
    check_expr env e2
  | EUnpack(x, z, v, e) ->
    begin match Type.view (check_value env v) with
    | TExists(y, tp1) ->
      begin match Kind.equal (TVar.kind x) (TVar.kind y) with
      | Equal ->
        let scope = Env.scope env in
        let (env, x') = Env.add_tvar env x in
        let tp1 = Type.subst_type y (Type.var x') tp1 in
        let env = Env.add_var env z tp1 in
        let (tp, eff) = check_expr env e in
        begin try
          if Type.check_scope scope eff then
            (Type.supertype_in_scope scope tp, eff)
          else
            raise TypeError
        with
        | Type.Escapes_scope -> raise TypeError
        end
      | NotEqual -> raise TypeError
      end
    | _ -> raise TypeError
    end
  | ETypeDef(tds, e) ->
    let scope = Env.scope env in
    let env = check_typedefs env tds in
    let (tp, eff) = check_expr env e in
    begin try
      if Type.check_scope scope eff then
        (Type.supertype_in_scope scope tp, eff)
      else
        raise TypeError
    with
    | Type.Escapes_scope -> raise TypeError
    end
  | ETypeApp(v, tp) ->
    begin match Type.view (check_value env v) with
    | TForall(x, tbody) ->
      begin match Kind.equal (TVar.kind x) (Type.kind tp) with
      | Equal ->
        (Type.subst_type x (tr_type env tp) tbody, Type.eff_pure)
      | NotEqual -> raise TypeError
      end
    | _ -> raise TypeError
    end
  | EInstApp(v, a) ->
    begin match Type.view (check_value env v) with
    | TForallInst(b, sb, tp) ->
      let (a, sa) = Env.lookup_inst env a in
      if not (Type.equal sa sb) then
        raise TypeError;
      (Type.subst_inst b a tp, Type.eff_pure)
    | _ -> raise TypeError
    end
  | EApp(v1, v2) ->
    begin match Type.view (check_value env v1) with
    | TArrow(tp1, tp2, eff) ->
      check_value_type env v2 tp1;
      (tp2, eff)
    | _ -> raise TypeError
    end
  | EProj(v, idx) ->
    begin match Type.view (check_value env v) with
    | TTuple tps ->
      if idx < List.length tps then
        (List.nth tps idx, Type.eff_pure)
      else
        raise TypeError
    | _ -> raise TypeError
    end
  | ESelect(proof, n, v) ->
    begin match Type.view (check_value env proof) with
    | TRecordDef(tp, flds) when (n < List.length flds) ->
      check_value_type env v tp;
      let (FieldDecl(_, tp)) = List.nth flds n in
      (tp, Type.eff_pure)
    | _ -> raise TypeError
    end
  | EMatch(proof, v, cls, rtp) ->
    begin match Type.view (check_value env proof) with
    | TDataDef(tp, ctors) when (List.length ctors = List.length cls) ->
      check_value_type env v tp;
      let rtp = tr_type env rtp in
      let eff = List.fold_left2 (fun eff cl ctor ->
          Type.eff_cons (check_match_clause env cl ctor rtp) eff
        ) Type.eff_pure cls ctors in
      (rtp, eff)
    | _ -> raise TypeError
    end
  | EHandle hdata ->
    begin match Type.view (check_value env hdata.proof) with
    | TEffsigDef(s, ops) when
        (List.length ops = List.length hdata.op_handlers) ->
      let (env_in, a) = Env.add_effinst env hdata.effinst s in
      let tp  = tr_type env hdata.htype in
      let eff = tr_type env hdata.heffect in
      let tp_in = check_expr_eff env_in hdata.body
        (Type.eff_cons (Type.eff_inst a) eff) in
      check_expr_type_eff (Env.add_var env hdata.return_var tp_in)
        hdata.return_body tp eff;
      List.iter2 (fun h op -> check_op_handler env h op tp eff)
        hdata.op_handlers ops;
      (tp, eff)
    | _ -> raise TypeError
    end
  | EOp(proof, n, a, targs, args) ->
    begin match Type.view (check_value env proof) with
    | TEffsigDef(s1, ops) when (n < List.length ops) ->
      let (a, s2) = Env.lookup_inst env a in
      if not (Type.equal s1 s2) then
        raise TypeError;
      let (OpDecl(_, xs, tps, res_tp)) = List.nth ops n in
      let sub = check_type_args env xs targs Subst.empty in
      let tps = List.map (Type.subst sub) tps in
      let res_tp = Type.subst sub res_tp in
      if List.length tps <> List.length args then
        raise TypeError;
      List.iter2 (check_value_type env) args tps;
      (res_tp, Type.eff_inst a)
    | _ -> raise TypeError
    end
  | ERepl(_, tp, eff, _) ->
    (tr_type env tp, tr_type env eff)
  | EReplExpr(e1, _, e2) ->
    let (tp, eff) = check_expr env e2 in
    let _ : ttype = check_expr_eff env e1 eff in
    (tp, eff)
  | EReplImport _ ->
    failwith "Not implemented"

and check_expr_type env e tp =
  let (tp', eff) = check_expr env e in
  if Type.subtype tp' tp then eff
  else raise TypeError

and check_expr_eff env e eff =
  let (tp, eff') = check_expr env e in
  if Type.subeffect eff' eff then tp
  else raise TypeError

and check_expr_type_eff env e tp eff =
  let (tp', eff') = check_expr env e in
  if not (Type.subtype tp' tp) then begin
    log_b (lazy (Box.textl "type mismatch"));
    log_b (lazy
      [ Box.ws (Box.word "got:")
      ; Box.ws (Type.to_sexpr tp' |> SExpr.pretty) ]);
    log_b (lazy
      [ Box.ws (Box.word "expected:")
      ; Box.ws (Type.to_sexpr tp |> SExpr.pretty) ]);
    raise TypeError
  end
  else if Type.subeffect eff' eff then ()
  else raise TypeError

and check_rec_functions env rfs =
  let prepare_rec_function env rf =
    match rf with
    | RFFun(f, tp, _, _, _, _) | RFInstFun(f, tp, _, _, _, _) ->
      let tp = tr_type env tp in
      (Env.add_var env f tp, (rf, tp))
  in
  let (env, rfs) = Utils.ListExt.fold_map prepare_rec_function env rfs in
  let check_rec_function (rf, tp) =
    let rftp =
      match rf with
      | RFFun(_, _, tvars, arg, arg_tp, body) ->
        let (env, tvars) = Env.add_tvars env tvars in
        let arg_tp = tr_type env arg_tp in
        let env = Env.add_var env arg arg_tp in
        let (body_tp, body_eff) = check_expr env body in
        Type.forall_l tvars (Type.arrow arg_tp body_tp body_eff)
      | RFInstFun(_, _, tvars, a, s, body) ->
        let (env, tvars) = Env.add_tvars env tvars in
        let s = tr_type env s in
        let (env, a) = Env.add_effinst env a s in
        let body_tp = check_expr_eff env body Type.eff_pure in
        Type.forall_l tvars (Type.forall_inst a s body_tp)
    in
    if Type.subtype rftp tp then ()
    else begin
      raise TypeError
    end
  in
  List.iter check_rec_function rfs;
  env

and check_match_clause env (Clause(ys, zs, body)) (CtorDecl(_, xs, tps)) rtp =
  let scope = Env.scope env in
  let (env, sub) = Env.add_tvars2 env ys xs in
  let env = Env.add_vars env zs (List.map (Type.subst sub) tps) in
  let eff = check_expr_type env body rtp in
  if Type.check_scope scope eff then eff
  else raise TypeError

and check_op_handler env h op tp eff =
  let (OpHandler(ys, zs, rx, body)) = h in
  let (OpDecl(_, xs, tps, rtp)) = op in
  let (env, sub) = Env.add_tvars2 env ys xs in
  let env = Env.add_vars env zs (List.map (Type.subst sub) tps) in
  let env = Env.add_var env rx (Type.arrow (Type.subst sub rtp) tp eff) in
  check_expr_type_eff env body tp eff 

and check_value env v =
  match v.data with
  | VLit lit -> Type.base (Lang.RichBaseType.type_of_lit lit)
  | VVar x ->
    Env.check_var env x
  | VFn(x, tp, body) ->
    let tp1 = tr_type env tp in
    let (tp2, eff) = check_expr (Env.add_var env x tp1) body in
    Type.arrow tp1 tp2 eff
  | VTypeFun(x, body) ->
    let (env, x) = Env.add_tvar env x in
    Type.forall x (check_expr_eff env body Type.eff_pure)
  | VInstFun(a, s, body) ->
    let s = tr_type env s in
    let (env, a) = Env.add_effinst env a s in
    Type.forall_inst a s (check_expr_eff env body Type.eff_pure)
  | VPack(ptp, v, x, tp) ->
    let ptp = tr_type env ptp in
    let (env', x) = Env.add_tvar env x in
    let tp = tr_type env' tp in
    check_value_type env v (Type.subst_type x ptp tp);
    Type.exists x tp
  | VTuple vs ->
    Type.tuple (List.map (check_value env) vs)
  | VCtor(proof, n, targs, args) ->
    begin match Type.view (check_value env proof) with
    | TDataDef(tp, ctors) when (n < List.length ctors) ->
      let (CtorDecl(_, xs, tps)) = List.nth ctors n in
      let sub = check_type_args env xs targs Subst.empty in
      let tps = List.map (Type.subst sub) tps in
      if List.length tps <> List.length args then
        raise TypeError;
      List.iter2 (check_value_type env) args tps;
      tp
    | _ -> raise TypeError
    end
  | VRecord(proof, vs) ->
    begin match Type.view (check_value env proof) with
    | TRecordDef(tp, flds) when (List.length vs = List.length flds) ->
      List.iter2 (fun v (FieldDecl(_, tp)) -> check_value_type env v tp)
        vs flds;
      tp
    | _ -> raise TypeError
    end
  | VExtern(name, tp) -> tr_type env tp

and check_value_type env v tp =
  let tp' = check_value env v in
  if Type.subtype tp' tp then ()
  else raise TypeError

let import (env, eff) im =
  let (env, _) = Env.add_tvars env im.im_types in
  let im_eff = tr_type env im.im_handle in
  let env = Env.add_var env im.im_var
    (Type.tuple (List.map (tr_type env) im.im_sig)) in
  (env, Type.eff_cons im_eff eff)

let check_program env p =
  let (env, eff) = List.fold_left import (env, Type.eff_pure) p.sf_import in
  match p.sf_body with
  | UB_Direct body ->
    let (env', xs) = Env.add_tvars env body.types in
    let tp =
      Type.exists_l xs (Type.tuple (List.map (tr_type env') body.body_sig)) in
    check_expr_type_eff env body.body tp eff
  | UB_CPS body ->
    let ambient_eff = tr_type env body.ambient_eff in
    let cps_tp      = TVar.fresh KType   in
    let cps_eff     = TVar.fresh KEffect in
    let (env', xs) = Env.add_tvars env body.types in
    let ans_eff    = Type.eff_cons (Type.var cps_eff) ambient_eff in
    begin if Type.subeffect ambient_eff eff then () else raise TypeError end;
    let tp =
      Type.forall cps_tp (Type.forall cps_eff
        (Type.arrow
          (Type.forall_l xs (Type.arrow
            (Type.tuple (List.map (tr_type env') body.body_sig))
            (Type.var cps_tp)
            (Type.eff_cons ans_eff (tr_type env' body.handle))))
          (Type.var cps_tp)
          ans_eff)) in
    check_expr_type_eff env body.body tp eff

(* ========================================================================= *)
let check_program p =
  try check_program Env.empty p; true with
  | TypeError -> false

let flow_transform p meta =
  Flow.return (check_program p)

let flow_tag =
  Flow.register_invariant_checker
    ~node:   Lang.Core.flow_node
    ~checks: CommonTags.well_typed
    ~name:   "Core well-typed"
    flow_transform
