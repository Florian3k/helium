
val use_colors : out_channel -> bool

val use_attributes     : out_channel -> Attr.t list -> unit
val default_attributes : out_channel -> unit
